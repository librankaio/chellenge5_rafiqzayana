const path = require("path");

module.exports = {
  home: (req, res) => {
    return res.send("Welcome to my Server");
  },
  info: (req, res) => {
    return res.send(`this is info page`);
  },
  bio: (req, res) => {
    return res.send(`Rafiq Zayana`);
  },
  product: (req, res) => {
    const id = req.params.id;
    return res.send(`id is:${id}`);
  },
  test: (req, res) => {
    res.sendFile(path.join(__dirname, "../views/index.html"));
  },
  api: (req, res) => {
    return res.json({
      id: 1,
      name: "Buku Cerita",
      price: 15000,
    });
  },
  login: (req, res) => {
    const dummyUser = {
      email: "sample@gmail.com",
      password: "sample123",
    };

    if (
      req.body.email === dummyUser.email &&
      req.body.password === dummyUser.password
    ) {
      return res.json(
        {
          message: "Login Success",
          data: dummyUser,
        },
        200
      );
    }

    return res.json(
      {
        message: "Login Failed",
      },
      400
    );
  },
};
