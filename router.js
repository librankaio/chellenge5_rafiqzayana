const express = require("express");
const router = express.Router();
const exampleCotroller = require("./controllers/exampleController");

router.get("/", exampleCotroller.home);

router.get("/info", exampleCotroller.info);
router.get("/bio", exampleCotroller.bio);
//Alter slug
router.get("/product/:id", exampleCotroller.product);

router.get("/test", exampleCotroller.test);

router.post("/login", exampleCotroller.login);

//slug
// router.get("/product/:id", (req, res) => {
//   const id = req.params.id;
//   return res.send(`id is:${id}`);
// });

module.exports = router;
