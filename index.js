const express = require("express");
const app = express();
const router = require("./router");

app.use(express.static(__dirname));
app.use(express.json()); //ini adalah config untuk menerima request json dari client
app.use(express.urlencoded({ extended: true })); // ini adalah config untuk menerima request selain json
// app.set("view engine", "ejs");
// app.set("view engine", "html");
app.use(router);

app.listen(8000, () => {
  console.log(`Server is up and running at ${8000}`);
});
